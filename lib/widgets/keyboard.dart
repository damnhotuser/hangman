import 'package:flutter/material.dart';

typedef KeypressCallback = void Function(String letter);

class Keyboard extends StatelessWidget {
  const Keyboard({@required this.onKeyPress, @required this.disabledLetters});

  // callback when key of the keyboard is pressed
  final KeypressCallback onKeyPress;
  final String disabledLetters;

  static final List<String> letters = "QWERTYUIOPASDFGHJKLZXCVBNM-".split('');

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width,
      child: Wrap(
        spacing: 8.0, // gap between adjacent chips
        runSpacing: 4.0,
        children: List.generate(letters.length, (index) {
          return ButtonTheme(
            minWidth: 50,
            child: RaisedButton(
              color: this.disabledLetters.contains(letters[index])
                  ? Colors.black
                  : Colors.grey,
              padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Text(letters[index]),
              onPressed: () {
                if (!this.disabledLetters.contains(letters[index]))
                  this.onKeyPress(letters[index]);
              },
            ),
          );
        }),
      ),
    );
  }
}
