import 'dart:math';

import 'package:flutter/material.dart';
import 'package:hangman/classes/player.dart';

Color getRandomColor() {
  return Colors.primaries[Random().nextInt(Colors.primaries.length)];
}


List<Player> sortedPlayers(List<Player> players){

  players.sort((a, b){
    return b.score.compareTo(a.score);
  });

  return players;
}