import 'package:hangman/classes/player.dart';

class ProposedLetter {
  String letter;
  Player player;

  ProposedLetter(final String letter, final Player player){
    this.letter = letter;
    this.player = player;
  }
}