import 'package:flutter/material.dart';

class Player {
  String name;
  int score = 0;
  Color color;

  Player(final String name, final Color color) {
    this.name = name;
    this.color = color;
  }

  int incrementScore(final int incrementBy) {
    if (incrementBy != null && incrementBy > 0)
      this.score += incrementBy;
    else
      this.score += 1;
    return this.score;
  }
}
