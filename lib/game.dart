import 'package:random_words/random_words.dart';
import 'package:flutter/material.dart';
import 'package:hangman/widgets/keyboard.dart';

import 'classes/player.dart';
import 'classes/proposed_letter.dart';
import 'scores.dart';

class GamePage extends StatefulWidget {
  GamePage({Key key, @required this.players}) : super(key: key);

  final List<Player> players;

  @override
  GamePageState createState() => GamePageState(this.players);
}

class GamePageState extends State<GamePage> {
  static const int NUMBER_OF_GAMES = 6;
  static const int POINTS_BY_WORD = 5;

  String word;

  List<Player> players;
  Player currentPlayer;
  int indexCurrentPlayer;

  List<ProposedLetter> proposedLetters = [];
  String disabledLetters = '';

  int failCount = 0;
  int gameCount = 1;
  bool canNextGame = false;

  final wordPropositionController = TextEditingController();

  GamePageState(List<Player> players) {
    this.players = players;
    this.players.shuffle();
    this.indexCurrentPlayer = 0;
    this.currentPlayer = this.players[indexCurrentPlayer];
    this.changeWord();
  }

  List<Widget> getLettersFromWord() {
    List<Widget> list = new List<Widget>();

    for (var c in word.toUpperCase().split('')) {
      ProposedLetter proposedLetter =
          proposedLetters.firstWhere((p) => p.letter == c, orElse: () => null);
      list.add(Text(
        proposedLetter != null ? proposedLetter.letter : "  ",
        style: TextStyle(
          decoration: TextDecoration.underline,
          color: proposedLetter != null
              ? proposedLetter.player.color
              : Colors.black,
          fontSize: 22,
        ),
      ));
    }

    return list;
  }

  void tryLetter(String c) {
    c = c.toUpperCase();

    setState(() {
      proposedLetters.add(new ProposedLetter(c, currentPlayer));
    });

    int number_of_letter =
        word.toUpperCase().split('').where((w) => w == c).length;
    if (number_of_letter <= 0) {
      fail();
    } else {
      currentPlayer.incrementScore(number_of_letter);
    }

    setState(() {
      this.disabledLetters += c;
    });


    if (failCount == 6) {
      printResponse();
      //nextGame();
    }
    else{
      bool found = true;
      for (var w in word.toUpperCase().split('')) {
        if (!proposedLetters.map((p) => p.letter).contains(w)) found = false;
      }

      if (found) {
        win();
      }

      nextPlayer();
    }
  }

  void fail() {
    setState(() {
      failCount++;
    });
  }

  void printResponse(){
    List<ProposedLetter> letters = [];

    for(var l in word.toUpperCase().split('')){
      if(!proposedLetters.map((p) => p.letter).contains(l)){
        letters.add(new ProposedLetter(l, currentPlayer));
      }
    }

    setState(() {
      proposedLetters.addAll(letters);
      canNextGame = true;
    });
  }

  void tryWord() {
    print(wordPropositionController.text.toUpperCase());
    if (wordPropositionController.text.toUpperCase() == word.toUpperCase()) {
      win();
    } else {
      fail();
      nextPlayer();
    }
    wordPropositionController.clear();
  }

  void nextPlayer() {
    setState(() {
      indexCurrentPlayer = ++indexCurrentPlayer % players.length;
      currentPlayer = players[indexCurrentPlayer];
    });
  }

  void changeWord() {
    word = generateNoun(safeOnly: false)
        .firstWhere((WordNoun w) => w.toString().length > 6, orElse: () => null)
        .toString();
    print(word);
  }

  void win() {
    currentPlayer.incrementScore(POINTS_BY_WORD);
    nextGame();
  }

  void nextGame() {
    if (gameCount != NUMBER_OF_GAMES) {
      setState(() {
        changeWord();
        proposedLetters = [];
        disabledLetters = '';
        failCount = 0;
        gameCount++;
        players.shuffle();
        indexCurrentPlayer = 0;
        currentPlayer = players[indexCurrentPlayer];
        canNextGame = false;
      });
    } else
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ScoresPage(players: this.players)));
  }

  _showDialog() async {
    await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return _SystemPadding(
            child: AlertDialog(
              contentPadding: const EdgeInsets.all(16.0),
              content: Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      autofocus: true,
                      controller: wordPropositionController,
                      decoration: InputDecoration(
                          labelText: 'The word', hintText: 'eg. Fornite'),
                    ),
                  )
                ],
              ),
              actions: <Widget>[
                FlatButton(
                    child: Text('SEND'),
                    onPressed: () {
                      tryWord();
                      Navigator.pop(context);
                    })
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Game " +
            gameCount.toString() +
            " of " +
            NUMBER_OF_GAMES.toString()),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    currentPlayer.name,
                    style: TextStyle(color: currentPlayer.color, fontSize: 22),
                  ),
                  Text(
                    ", your turn.",
                    style: TextStyle(
                      fontSize: 22,
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    child: Image.asset(
                        'images/hangman' + failCount.toString() + '.png'),
                    decoration: BoxDecoration(
                      border: Border.all(color: currentPlayer.color),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: getLettersFromWord(),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RaisedButton(
                    child: Text('Propose a word'),
                    onPressed: () {
                      _showDialog();
                    },
                  ),
                  canNextGame ? RaisedButton(
                    child: Text('Next Game'),
                    onPressed: () {
                      nextGame();
                    },
                  ): SizedBox()
                ],
              ),
            ),
            Keyboard(
              onKeyPress: (String letter) {
                this.tryLetter(letter);
              },
              disabledLetters: this.disabledLetters,
            )
          ],
        ),
      ),
    );
  }
}

class _SystemPadding extends StatelessWidget {
  final Widget child;

  _SystemPadding({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return new AnimatedContainer(
        padding: mediaQuery.viewInsets,
        duration: const Duration(milliseconds: 300),
        child: child);
  }
}
