import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hangman/classes/player.dart';

import 'helpers/utils.dart';


class ScoresPage extends StatefulWidget {
  ScoresPage({Key key, this.players}) : super(key: key);

  final List<Player> players;

  @override
  _ScoresPage createState() => _ScoresPage();
}

class _ScoresPage extends State<ScoresPage> {

  TextStyle getTextStyle(Player player) {
    return TextStyle(
      color: player.color,
      fontSize: 30,
    );
  }

  List<Widget> ordering(List<Player> p){
    List<Player> players = sortedPlayers(p);

    List<Widget> widgets= new List();

    header(players, widgets);
    for(var i = 0; i < players.length; i++){
      Player p = players[i];
      int position = i+1;
      widgets.add(
          Padding(
            padding: const EdgeInsets.all(10),
            child: Text('$position - ' + p.name + ' : ' + p.score.toString(), style: getTextStyle(p)),
          ),
      );
    }

    return widgets;
  }

  void header(List<Player> players, List<Widget> widgets){
    widgets.add(
      Padding(padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('The Winner is ' + players[0].name.toUpperCase(), style: getTextStyle(players[0]))
          ],
        ),
      ),
    );
    widgets.add(
      Padding(
        padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child: Image.asset(
                  'images/cracker.gif', width: 200),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: this.ordering(widget.players)
          ,
        ),
      ),
    );
  }
}
