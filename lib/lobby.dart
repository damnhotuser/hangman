import 'package:flutter/material.dart';
import 'package:hangman/classes/player.dart';
import 'package:hangman/game.dart';
import 'package:hangman/helpers/utils.dart';

class LobbyPage extends StatefulWidget {
  LobbyPage({Key key}) : super(key: key);

  @override
  _LobbyPageState createState() => _LobbyPageState();
}

class _LobbyPageState extends State<LobbyPage> {
  static const int MAX_LOBBY_SIZE = 5;

  final newPlayerInputController = TextEditingController();

  List<Player> _players = new List();

  // called when clicking on the '+' button near the input field
  bool addPlayerToList(final String name) {
    if (name.length > 0 && this._players.length < MAX_LOBBY_SIZE) {
      setState(() {
        this._players = [...this._players, Player(name, getRandomColor())];
        this.newPlayerInputController.clear();
      });
      return true;
    }
    return false;
  }

  // called when clicking the cross near each player line
  void removePlayerFromList(final Player player) {
    setState(() {
      this._players.remove(player);
      //this._players = this._players;
    });
  }

  List<Widget> getPlayersListAsWidgets() {
    List<Widget> list = new List();
    for (var p in this._players) {
      list.add(Padding(
        padding: EdgeInsets.fromLTRB(2, 8, 2, 8),
        child:
        Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          Icon(Icons.supervised_user_circle, color: p.color, size: 40.0),
          Padding(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Text(p.name,
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ))),
          FloatingActionButton(
            mini: true,
            heroTag: "remove" + p.name,
            onPressed: () {
              this.removePlayerFromList(p);
            },
            backgroundColor: Colors.white,
            child: Icon(Icons.cancel, color: Colors.grey, size: 20.0),
          ),
        ]),
      ));
    }

    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Declare players (' +
            this._players.length.toString() +
            '/' +
            MAX_LOBBY_SIZE.toString() +
            ' max.)'),
        backgroundColor: Colors.deepOrange,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ...this.getPlayersListAsWidgets(),
            Padding(
                padding: EdgeInsets.fromLTRB(60, 30, 60, 80),
                child: this._players.length < MAX_LOBBY_SIZE
                    ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Flexible(
                      child: TextField(
                        controller: this.newPlayerInputController,
                        decoration:
                        InputDecoration(hintText: 'ex: John'),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                        child: FloatingActionButton(
                          mini: true,
                          heroTag: "add",
                          onPressed: () {
                            this.addPlayerToList(
                                this.newPlayerInputController.text);
                          },
                          backgroundColor: Colors.deepOrange,
                          child: Icon(Icons.add),
                        ))
                  ],
                )
                    : null),
            Builder (
              builder: (context) => RaisedButton(
                onPressed: () {
                  if(_players.length > 0)
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) =>
                            GamePage(players: this._players)));
                  else
                    Scaffold.of(context).showSnackBar(
                        SnackBar(
                          content: Text('You need at least one player !'),
                          duration: Duration(seconds: 3),
                        )
                    );
                },
                color: Colors.deepOrange,
                child: Text('Launch Game'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    this.newPlayerInputController.dispose();
    super.dispose();
  }
}
