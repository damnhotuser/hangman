import 'package:flutter/material.dart';
import 'package:hangman/lobby.dart';
import 'package:hangman/scores.dart';

import 'classes/player.dart';

void main() {
  runApp(MaterialApp(
    title: 'Named Routes Demo',
    // Start the app with the "/" named route. In this case, the app starts
    // on the FirstScreen widget.
    initialRoute: '/',
    routes: {
      // When navigating to the "/" route, build the FirstScreen widget.
      '/': (context) => LobbyPage(),
      '/score': (context) => ScoresPage(players : players()),
      // When navigating to the "/second" route, build the SecondScreen widget.
      //'/second': (context) => SecondScreen(),
    },
  ));
}

List<Player> players(){
  Player p = new Player('pepe', Colors.amber);
  Player p2 = new Player('meme', Colors.black);
  Player p3 = new Player('tete', Colors.blue);
  p.incrementScore(10);
  p2.incrementScore(5);

  List<Player> players = new List<Player>();
  players.add(p);
  players.add(p2);
  players.add(p3);

  return players;
}